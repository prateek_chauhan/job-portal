export class UserInfoModel{
    constructor(){
        this.fullName = ''
        this.mobileNumber = ''
        this.email = ''
        this.dateOfBirth = new Date()
        this.totalExperience = ''
        this.resume = ''
    }
    public fullName:String;
    public mobileNumber:String;
    public email:String;
    public dateOfBirth: Date;
    public totalExperience: String;
    public resume: String;

}