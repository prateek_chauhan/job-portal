export class UserModel{
    constructor(){
        this.firstName = ''
        this.lastName = ''
        this.contact = ''
    }
    public firstName:String;
    public lastName:String;
    public contact:String;
}