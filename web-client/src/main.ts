import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store/store'
import VeeValidate from 'vee-validate';
// Vue.use(VeeValidate); //For validatio/n

import '@/assets/scss/main.scss';
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
