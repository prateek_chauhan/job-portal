import config from '@/environments/index';
import Vue from 'vue';

/**
 * The backend server's base api url based on environment.
 */
export const BASE_API_URL = config.SERVER_BASE_URL;

/**
 * Disable production tips
 */
Vue.config.productionTip = false;
/**
 * Hide VueJs dev tools
 */
Vue.config.devtools = false;

/**
 * The config options for Snotify.
 */
export const snotifyOptions = {
    toast: {
        position: 'rightBottom',
        showProgressBar: false
    }
}


/**
 * Exporting EventBus for
 */
export const EventBus = new Vue();
