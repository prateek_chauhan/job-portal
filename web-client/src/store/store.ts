import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import { VuexPersistence } from 'vuex-persist'
import {UserInfoModel} from '../models/user-info.model'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersistence({
  key: 'state.jobPortal',
  storage: window.localStorage
})
export const userData = () => {
  return {
    UserInfo: new UserInfoModel()
  }
}
export const initialState = () => ({
  sessionObject: {},
  userInfo: userData
})
export default new Vuex.Store({
  state: initialState,
  strict: false,
  mutations: mutations,
  actions: actions,
  modules: {
  },
  plugins: [vuexLocalStorage.plugin]
})
