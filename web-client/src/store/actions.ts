export default {
    setSession({ commit }: any, loginResponse: any) {
        commit('SET_LOGIN_SESSION', loginResponse)
    },

    clearSession({ commit }: any) {
        commit('CLEAR_SESSION')
    },

    updateSessionData({ commit }: any, data: any) {
        commit('UPDATE_SESSION_DATA', data)
    }
}