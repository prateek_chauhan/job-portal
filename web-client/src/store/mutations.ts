import {
    initialState,
} from "./store";

const SET_LOGIN_SESSION = (state: any, loginResponse: any) => {
    state.sessionObject = loginResponse;
};


const CLEAR_SESSION = function (state: any) {
    const initial: any = initialState();
    Object.keys(initial).forEach(key => {
        state[key] = initial[key];
    });
};

const UPDATE_SESSION_DATA = function (state: any, data: any) {
    state.sessionObject["userInfo"] = data;
};




export default {
    SET_LOGIN_SESSION,
    CLEAR_SESSION,
    UPDATE_SESSION_DATA
};
