import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  {
    path: '/',
    name: 'signup',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "Login" */ '../components/userspage/signup/SignUp.vue')
  },
  {
    path: '/activation-page',
    name: 'Activation-Page',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "activation" */ '../components/userspage/activation/activationPage.vue')
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "activation" */ '../components/userspage/login/Login.vue')
  },
  {
    path: '/user-details',
    name: 'UserDetails',
    component: () => import(/* webpackChunkName: "activation" */ '../components/userspage/user-details/UserDetails.vue')
  }
]

const router = new VueRouter({
  mode:'history',
  routes
})

export default router
