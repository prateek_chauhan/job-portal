// const commonUtil = require("../utils/common");
// const s3Config = require("../config/aws.config");
const moment = require('moment');

const emailTemplate = function (type, data) {
    let title = data.text
    if (type == 'ScenarioDeskEmailTemplate') {
        title = ` New Scenario from   ${data.brokerName}`;
    }

    var template = `
    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns:v="urn:schemas-microsoft-com:vml">
        <div>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
            <body>
                <style>
                    *{
                        padding: 0;
                        margin: 0;
                    }
                </style>
    
                <table width="800" style=" padding:0px 15px 30px 15px;border:1px solid #000;margin:0 auto;">
                   <tbody>
                        <tr> 
                            <td>
                                <table cellSpacing="0" cellPadding="0" align="middle" border="0" style="width: 552px;margin: 0 auto;padding-top:30px">
                                    <tbody>
                                        <tr> 
                                            <td  style="background-color:#fff;width:600px;text-align:center;border-bottom:1px solid #979797;padding-bottom: 20px;">
                                                <h1>Job Portal</h1>
                                            </td> 
                                        </tr> 
                                    </tbody>
                                </table>

                                <table cellSpacing="0" cellPadding="0" align="middle" border="0" width="552" style="background:#fff;margin:0 auto">
                                    <tbody>
                                        <tr>
                                            <td style="padding:35px 0px 35px 0px;font-size:30px;font-weight:bold;color:#000"> ${title} </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;">   ${getEmailBody(type, data)}</td>
                                        </tr>
                                    <tbody>
                                </table>

                                <table cellSpacing="0" cellPadding="0" align="middle" border="0" width="552" style="background:#fff;margin:0 auto;border-top: 1px solid black;padding-top: 30px">
                                    <tbody>
                                        <tr>
                                            <td style="font-size: 12px;text-align: center; color:#000">
                                                <div> &copy; ${moment().format('YYYY')}
                                                    <span style="font-weight: bold;">Job Portal.</span>
                                                    | All Rights Reserved.
                                                </div>
                                                <div>        
                                                    <span style="font-size: 10px ;">+91(XXX)XXX-XXXX</span><span>&nbsp;&nbsp;</span><span style="font-size: 12px;">www.wemlo.io</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;padding-top:10px">
                                                <span style="cursor: pointer;">
                                                    <a href="https://www.facebook.com/WEMLO-351723185560189/">
                                                        <img src="https://wemlo.s3.amazonaws.com/Common/icons/facebook.png">
                                                    </a>
                                                </span>
                                                <span>&nbsp;&nbsp;</span>
                                                <span style="cursor: pointer;">
                                                    <a href="https://www.linkedin.com/company/wemlo/"> 
                                                        <img src="https://wemlo.s3.amazonaws.com/Common/icons/linkedin.png">
                                                    </a>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table> 
            </body>
       </div>
</html>
    `;
    return template;
};
function getEmailBody(type, data) {
    let body = "";
    switch (type) {
        case "emailVerification":
            body = `
            <tr>
                <td style="padding:0px;">
                </td>
            </tr>
            <tr>
                <td style="padding: 15px 0px 0px 0px;">
                    <div style="color: #000;font-size:16px;">
                        Please click on the button below to complete the verification process. 
                    </div>
                </td> 
            </tr>
            <tr>
                <td style="padding:10px 0px 10px 0px">
                    <div style="color: #000;font-size: 16px;">
                        Feel free to contact us if you have any question at XXX-XXX-XXXX.
                    </div>
                </td> 
            </tr>
            <tr>
                <td style="padding:15px 0px 0px 0px;">
                    <div style="margin-top: 10px;margin-bottom: 0px;font-size: 20px;color: #000;">
                        Best regards,
                    </div>
                    <div style="color: #000;font-weight: bold;font-size: 20px;margin-bottom:0px;">Job Portal Team</div>
                </td> 
            </tr>
            <tr>
                <td style="padding:20px 0px 40px 0px;">
                    <a href="${data.link}" style="text-decoration: none">
                    <div style="background-color:#00b6ff;cursor:pointer;border:1px solid #00b6ff;border-radius:4px;color:#ffffff; 
                    text-align:center;font-size:16px;font-weight:bold;color:#fff">
                    <!--[if mso]>
                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word"
                     href="${data.link}" style="height:48px;v-text-anchor:middle; width:686px; arcsize="5%" strokecolor="#00b6ff" fillcolor="#00b6ff">
                    <w:anchorlock/>
                    <center style="color:#ffffff;font-family:Helvetica, Arial,sans-serif;font-size:16px;"> CONFIRM EMAIL</center>
                    </v:roundrect>
                    <![endif]-->
                    <span style="font-family:sans-serif;line-height:40px;text-decoration:none;-webkit-text-size-adjust:none;mso-hide:all;color:#fff"> 
                    CONFIRM EMAIL </span>
                    </div></a>
                </td>
            </tr>
            `;
    break;
}
return body;
}




module.exports = {
    emailTemplate
};
