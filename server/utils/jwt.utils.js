const jwt = require('jsonwebtoken')
const jwtConfig = require('../config/jwt.config')
const bcrypt = require('bcrypt')


const verifyAuthenticateToken = async function (request, response, next) {
    try {
        /**To get the token of request header  */
        let token = request.header['authorization']


        /**To check a reuest have a tokenOrNot */
        if (!token) return response.status(401).send({
            auth: false,
            message: 'Token not provided'
        })

        /**to decode a token and find out the userId  */
        let decodedToken = jwt.decode(token)
        let userId = decodedToken.id

        /**To find the userId in database   */
        let validUser = await ModelName.findOne({ _id: userId })

        /**to check userId is validOrNot */
        if (!validUser) return response.status(401).send({
            auth: false,
            message: 'Invalid user'
        })

        /**To verify a jwt token   */
        jwt.verify(token, jwtConfig.jwt.secret, function (error) {

            /** To cheeck the token is not expired  */
            if (error && error.name === 'TokenExpiredError')
                return response.status(401).send({
                    code: 'TokenExpired',
                    message: 'Token Expired'
                })
            /** To cheeck the token is not equal to expired  token  */
            if (error && error.name != 'TokenExpiredError')
                return response.status(401).send({
                    message: 'To failed to authenticate'
                })
        })
        /**If all coditions are false then to set the userId in request   */
        req['userId'] = userId
        next();
    } catch (error) {
        console.log('verifyAuthenticateToken', error)
    }
}
const generateJwtToken = async function (userId, validity) {
    token = jwt.sign(userId, jwtConfig.jwt.secret, { expiresIn: validity })
    return token;

}
const convertPasswordInBcrypt = async function (password) {
    return await bcrypt.hash(password, await bcrypt.genSalt(10));
}
const comparePassword = async function (savedPassowrd, requestPassowrd) {
    return await bcrypt.compare(savedPassowrd, requestPassowrd);
}


module.exports = {
    verifyAuthenticateToken,
    comparePassword,
    convertPasswordInBcrypt,
    generateJwtToken
}