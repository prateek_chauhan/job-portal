const manager = require("../../managers/authentication/authentication.manager");

const signUp = function (request, response) {
    manager.signUp(request.body, request.userId)
        .then(result => response.status(result.code).send(result.data))
        .catch(error => response.status(500).send(error.message))
}

const verifyActivationToken = function (request, response) {
    manager.verifyActivationToken(request.body, request.userId)
        .then(result => response.status(result.code).send(result.data))
        .catch(error => response.status(500).send(error.message))
}
const setPassword = function (request, response) {
    manager.setPassword(request.body, request.userId)
        .then(result => response.status(result.code).send(result.data))
        .catch(error => response.status(500).send(error.message))
}
const login = function (request, response) {
    manager.login(request.body, request.userId)
        .then(result => response.status(result.code).send(result.data))
        .catch(error => response.status(500).send(error.message))
}

module.exports = {
    signUp,
    verifyActivationToken,
    setPassword,
    login
}