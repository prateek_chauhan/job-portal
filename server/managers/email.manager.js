
/**
 * 
 * @createdBy - Pratik chauhan   @date = 22/04/2020 
 * 
 */
const MailConfig = require('../config/mail.config')
const Logger = require('../config/logger.config');
const Template = require('../utils/emailTemplates');

const sendInvitationLink = async function(userInfo,userId){
    try {
        MailConfig.transporter.sendMail({
            from:MailConfig.sender,
            to:userInfo.email,
            subject:'Job Portal Verify Link',
            html: Template.emailTemplate('emailVerification', { link: userInfo.link, text: 'Verify Email' })
        })

        MailConfig.transporter.close()
    } catch (error) {
        Logger.log('sendInvitationLink',error,userId)
    }
}
module.exports ={
    sendInvitationLink
}