
/**
 * 
 * @createdBy - Kishan kumar   @date = 21/04/2020  
 * sir please write the all files when you are create a file and API.bcause it will help 
 * to a future AND please this follow of structure when you read this msg then u delete this msg 
 * 
 */
/**
 * 
 * There is imported a some important files  
 */
const Logger = require('../../config/logger.config')
const crypto = require('crypto')
const ResponseUtil = require('../../utils/response.util')
const UserModel = require('../../models/authentication/user.model')


/**   to create a APIs */
const signUp = async function (body, userId) {
    try {
       let userData =  await  require("../../models/authentication/user.model")(
           {contactType: body.contact,userType: "user", firstName: body.firstName,lastName: body.lastName}).save();
           console.log(userData)
        await saveUserToken({email:userData.contactType,userId:userData._id})
        return ResponseUtil.success({})
    } catch (error) {
        Logger.log('signUp', error, userId)

    }
}

/**
 * 
 *  @createdBy -Prateek Chauhan @date = 22/04/2020  
 * 
 */
const saveUserToken = async function(userData){
    try {
        /**To create a token  */
        let token  = crypto.randomBytes(64).toString('hex') + userData.userId
        /**To create a validty time of token  */
        let validityTokenTime = new Date().setMinutes(new Date().getMinutes() + 86400)
        /***To update a token in token collection  */
        let tokenInfo = { token:token,expiresAt:validityTokenTime,userId:userData.userId,email:userData.email}
        await new  require('../../models/authentication/token.model')(tokenInfo).save()
        //**To send Invitation link to activate a userAcount  */
        userData['token'] = token
        await sendUserInvitationLink(userData,userData.userId)        
    } catch (error) {
        Logger.log('saveUserToken',error,userData.userId)
    }
}

/**
 * 
 *  @createdBy -Prateek Chauhan @date = 22/04/2020  
 * 
 */
const sendUserInvitationLink = async function (userData, userId) {
    try {

        /**To genrate a invitation url    */
        const invitationUrl  = await require('../../utils/commonFunction').getDomainUrl('activation-page?userToken='+userData.token)
        /** To calling a function to send a email and passing a two parameter userData and userId */
        require('../../managers/email.manager').sendInvitationLink({ email: userData.email, link: invitationUrl }, userId)
    } catch (error) {
        Logger.log('sendUserInvitationLink', error, userId)
    }
}
/**
 * To verify the activation token 
 * @param {} body 
 */
const verifyActivationToken = async function (body) {
  try {
    let currentDate = new Date(new Date().toLocaleDateString());
    let tokenInfo = await require("../../models/authentication/token.model").findOne({ token: body.token });
    if (!tokenInfo) {
      const data = {
        status: "error",
        message: "InValid token"
      };
      Logger.log("verifyActivationToken", data.message, "verifyActivationToken");
      return ResponseUtil.success(data);
    }
    if (currentDate <= tokenInfo.expiresAt) {
      let user = await require('../../models/authentication/user.model').findOne({ _id: tokenInfo.userId });
      if (user) {
        const data = {
          activationToken: body.token,
          contactType: user.contactType,
          userId: user._id,
          type: user.userType
        };
        Logger.log("verifyActivationToken", data.message, "verifyActivationToken");
        return ResponseUtil.success(data);
      }
      else {
        await require("../../models/authentication/token.model").deleteOne({ token: body.token });
        const data = {
            status: "error",
            message: "The link has been expired."
        };
        Logger.log("verifyActivationToken", data.message, user.email);
        await TokenModel.deleteOne({ token: body.token });
        return ResponseUtil.success(data);
      }
    } else {
      const data = {
        status: "error",
        message: "The link has been expired."
      };
      Logger.log("verifyActivationToken", data.message, user.email);
      await require("../../models/authentication/token.model").deleteOne({ token: body.token });
      return ResponseUtil.success(data);
    }
  } catch (error) {
    Logger.log('verifyActivationToken',error)
  }
}

const setPassword = async function(body){
  try {
    let bcryptPassword= await require('../../utils/jwt.utils').convertPasswordInBcrypt(body.userInfo.password)
    console.log(bcryptPassword)
   let userId = await UserModel.updateOne({email:body.email},
    {$set:{password:bcryptPassword,isEmailVerified:true,userActive:true}})
    // await require("../../models/authentication/token.model").deleteOne({userId:userId})
    console.log(userId)

    return ResponseUtil.success({message:'Sucessfully'})
  } catch (error) {
    Logger.log('setPassword',error)    
  }
}

const login = async function(body){
  try {
    // console.log(body)
    let user =  await UserModel.findOne({contactType:body.email})
    console.log(user)
    if(!user){
    const data ={
      status:"error",
      type:'email',
      message:'Email does not exists',
    }
    return ResponseUtil.success(data)
    }
    let comparePassword = await require('../../utils/jwt.utils').comparePassword(user.password,body.password)
    console.log(comparePassword)
    if(!comparePassword){
      const data = {
        status:"error",
        type:"password",
        message:'Password did not match'
      }
      return ResponseUtil.success(data)
    }
    if(!user.userActive){
      const data = {
        status:"error",
        type:"userNotActive",
        message:'Your account is deactivated ! Please firstly activate account'
      }
      return ResponseUtil.success(data)
    }
    
    let data ={}
    data  ={
      token :await require('../../utils/jwt.utils').generateJwtToken(user._id,86400),
      userData: user
    }

    return ResponseUtil.success(data)
  } catch (error) {
    Logger.log('setPassword',error)    
  }
}
 
module.exports = {
    signUp,
    verifyActivationToken,
    setPassword,
    login
}