const express = require("express");
const port = 8085;
const app = express();
const bodyParse =  require('body-parser');
const path = require('path');
const http = require('http')
require("./config/logger.config");






/**----------------------------------  the router files are require here  --------------------------*/
const authenticationRoute =  require('./routes/authentication.route')
require('./config/db.config')




/**  *************************** To set a route name to call a api on web-client  ******************************************** */
app
    /**To set the limit of json and urlencoded  body parse  */
    .use(bodyParse.urlencoded({limit:"100mb",extended:false}))
    .use(bodyParse.json({limit:"50mb", extends:true}))

    /** we are use the express static because the express static is middle ware to serve the static files like CSS,Images and Javascripts(static) function*/
    .use(express.static("public"))

    /**there are given a all acesss to call a api  */
    .use((request,response,next) =>{
        response.header("Access-Control-Allow-Origin", "*")
        response.header("Access-Control-Allow-Methods",
        "GET,HEAD,OPTIONS,POST,PUT,DELETE")
        response.header(
            "Access-Control-Allow-Headers",
            "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorization, refreshToken"
          );
         
     next();
    })

    /*****************APIs Call here    *****************************************  */
    .use('/authentication',authenticationRoute)

    /***Static items  */
    .use("/images", express.static(__dirname + "/images"))
    .listen(port, () => {
      console.info(`Job portal server is running at http://localhost:${port}`);
    });
    http.createServer(app)
