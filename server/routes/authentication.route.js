const routes = require('express').Router();
const controller = require("../controllers/authentication/authentication.controller")

routes.post("/signUp", controller.signUp);
routes.post("/verifyActivationToken", controller.verifyActivationToken);
routes.post("/setPassowrd",controller.setPassword);
routes.post("/login",controller.login)





module.exports =routes 