const nodeMailer = require('nodemailer')

/*** configuration of email and password  */

let config = {
    host: "smtp.office365.com",
    port: 587,
    secureConnection: false, // secure:true for port 465, secure:false for port 587
    // pool: true,
    // maxConnections: 100,
    StartTLS: 'Enabled',
    debug: true,
    tls: {
      ciphers: 'SSLv3'
    },
    auth: {
        user: 'jobportalhire@outlook.com',
        pass: 'jobportal@123'
    }
}
const transporter = nodeMailer.createTransport(config)

const sender = config.auth.user;
module.exports = {
    transporter,
    sender
}