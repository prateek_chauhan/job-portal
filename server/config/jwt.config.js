const crypto = require('crypto')

const jwt = {
    secret:crypto.randomBytes(64).toString('base64').replace(/\//g,'_').replace(/\/+/g,'_'),
    session:{
        session:false
    }
}
module.exports = jwt 