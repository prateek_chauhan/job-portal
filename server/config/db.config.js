const mongoose = require('mongoose')

const connectionUrl = 'mongodb://localhost:27017/job_portal'

mongoose.connect(connectionUrl,{
    useNewUrlParser: true,
    useCreateIndex: true,
    poolSize:20 ,
    useUnifiedTopology:true
}).then(() => {
    console.log('DB connection established to job portal')
}).catch(error=> {
    console.log(error)
    console.error('Could not DB connection established ')
})
