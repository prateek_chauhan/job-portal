"use strict";
const winston = require("winston");
const fs = require("fs");
const logDir = "log";
const { createLogger, format } = require("winston");
const { combine, prettyPrint } = format;
var moment = require('moment');
const tsFormat = () => new Date().toLocaleTimeString();
const tsFormats = moment().format('DD MMM YYYY HH:mm:ss x');

const log = function (functionName, err) {

    try {
        if (!fs.existsSync(logDir)) {
            fs.mkdirSync(logDir);
        }

        let errorLogObj = {
            Time: tsFormats,
            level: "debug",
            functionName: `${functionName}`,
            stack: err,

        }

        const errorLogger = createLogger({
            format: combine(prettyPrint()),
            transports: [
                // colorize the output to the console
                new winston.transports.Console({
                    timestamp: tsFormat,
                    colorize: true,
                    level: "debug"
                }),
                new (require("winston-daily-rotate-file"))({
                    filename: `${logDir}/log-`,
                    timestamp: tsFormat,
                    datePattern: "YYYY-MM-DD",
                    prepend: true,
                    level: "debug"
                })
            ]
        });
        if (err) {
            errorLogger.log(errorLogObj);
        }
    } catch (error) {
        console.log(error);
    }
};


module.exports = {
    log
}