const mongoose  = require('mongoose')
const Schema = mongoose.Schema;


let tokenSchema = new Schema({
        userId:{type:Schema.Types.ObjectId},
        email:{type:String},
        token:{type:String},
        expiresAt :{type:String}
})
module.exports = mongoose.model('token',tokenSchema)