const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let userSchema = new Schema({
    firstName: {type: String},
    lastName: {type: String},
    contactType: { type: String},
    phone: { type: String },
    password: { type: String },
    isEmailVerified: { type: Boolean, default: false },
    isPhoneVerified: { type: Boolean, },
    userType: { type: String },
    userActive: { type: Boolean, default: false },
});

module.exports = mongoose.model('User', userSchema)